from functools import reduce
from math import ceil

def print_kgrams(k: int, n: int=10):
    counts = {}
    for i in range(len(ciphertext)-(k-1)):
        s = ciphertext[i:i+k]
        counts[s] = counts[s]+1 if s in counts else 1
    print(sorted(counts.items(), key=lambda x: x[1], reverse=True)[:n])

def print_rep(k: int, n: int=10):
    counts = {}
    for i in range(len(ciphertext)-(k-1)):
        s = ciphertext[i:i+k]
        if s[0] == s[-1]:
            key = s[0] + '_'*(k-2) + s[-1]
            counts[key] = counts[key]+1 if key in counts else 1
    print(sorted(counts.items(), key=lambda x: x[1], reverse=True)[:n])

def print_rep2(k: int, n: int=10):
    counts = {}
    for i in range(len(ciphertext)-(k-1)):
        s = ciphertext[i:i+k]
        if s[0] == s[-1]:
            key = ''.join([s[i] if s[i] == s[0] else '_' for i in range(len(s))])
            counts[key] = counts[key]+1 if key in counts else 1
    print(sorted(counts.items(), key=lambda x: x[1], reverse=True)[:n])

def print_ddoubles(n: int=10):
    counts = {}
    for i in range(len(ciphertext)-3):
        s = ciphertext[i:i+4]
        if s[0] == s[1] and s[2] == s[3]:
            counts[s] = counts[s]+1 if s in counts else 1
    print(sorted(counts.items(), key=lambda x: x[1], reverse=True)[:n])

def print_step(ciphertext: int, step: int):
    print(''.join([ciphertext[i*step] for i in range(len(ciphertext) // step)]))

ciphertext = "LDBGUMADBFSBBTXGCHTYCCBXABUMGCCHSMBXBUVBMAHRBRBDBGMBMIDGAJHUXGLMADBHTBIDHDGMXEHOBTJLUXAMHAADGAXTHTBHJCHSURSXLTBXXNHQQGAIHXGLMXBVBTMHACBXNHQQGLALXDLXRSXLTBXXXGLMJLVBNHQQGGTMLYYABYYDLQLAIGXJHURULTKLTKADBNHHOASYLEUHHAXLTXABGMHJHTLHTXMHAXBVBTJYSTKMHITDLXRUSXDNHQQGGTMDGMPSXARBKSTIBYYNHQQGHJGYYADBSTPSXAADLTKXIDBTDLXBCBNDGTNBMAHJGYYSEHTGYLNBNHQQGGXXDBXAHHMIGANDLTKADBQNHQQGGTMDBNDBNOBMDLQXBYJXSMMBTYCADBHADBUXYHHOBMUHSTMGYXHNHQQGGTMGYYHJADBQRHIBMYHI"
print("len:", len(ciphertext))
counts = {}
for c in range(ord('A'), ord('Z')+1):
    counts[chr(c)] = ciphertext.count(chr(c))
print(sorted(counts.items(), key=lambda x: x[1], reverse=True))
# print('\n'.join(sorted(counts.keys(), key=lambda x:counts[x], reverse=True)))

print_kgrams(2, 20)
print_kgrams(3)
print_kgrams(4)
print_kgrams(5)
print_kgrams(6)
print_kgrams(7)
print_kgrams(8)
print_kgrams(9)

print_rep2(2)
print_rep2(3)
print_rep2(4)
print_rep2(5)
print_rep2(6)

print_ddoubles()

print('NHQQG:', ciphertext.count('NHQQG'))
print('NHQQGG:', ciphertext.count('NHQQGG'))
print('NHQQGGTM:', ciphertext.count('NHQQGGTM'))

# for i in range(1, 27):
#     print(i)
#     for j, c in enumerate(ciphertext):
#         print(chr((ord(c) - ord ('A') + i) % 26 + ord('A')), end='')
#     print()

for ci in range(ord('A'), ord('Z')+1):
    c = chr(ci)
    cs = set()
    for i in range(len(ciphertext)-1):
        if ciphertext[i] == c:
            cs.add(ciphertext[i+1])
    if len(cs) <= 2:
        print(c, f'({counts[c]})', cs)

mapping = {
#   src  ?? dst
    'B':    'e',
    'H':    'o',
    'X':    's',
    'G':    'a',
    'A':    't',
    'T':    'n',
    'D':    'h',
    'M':    'd',
    'L':    'i',
    'Y':    'l',
    'Q':    'm',
    'N':    'c',
    'S':    'u',
    'U':    'r',
    'J':    'f',
    'I':    'w',
    'C':    'y',
    'R':    'b',
    'K':    'g',
    'O':    'k',
    'V':    'v',
    'E':    'p',
    'P':    'j',
    'F':    'q',
    'W': 'X',
    'Z': 'Z',
    ' ': ' '
}

# mapping = {
# #   src  ?? dst
#     'A':    'T',
#     'B':    'E',
#     'C':    'Y',
#     'D':    'H',
#     'E':    'P',
#     'F':    'Q',
#     'G':    'A',
#     'H':    'O',
#     'I':    'W',
#     'J':    'F',
#     'K':    'G',
#     'L':    'I',
#     'M':    'D',
#     'N':    'C',
#     'O':    'K',
#     'P':    'J',
#     'Q':    'M',
#     'R':    'B',
#     'S':    'U',
#     'T':    'N',
#     'U':    'R',
#     'V':    'V',
#     'W':    'X',
#     'X':    'S',
#     'Y':    'L',
#     'Z':    'Z',
#     ' ': ' '
# }

print("==================================")
# plaintext = ""
# for i, c in enumerate(ciphertext):
#     plaintext += mapping[c]
plaintext = ''.join(map(lambda x: mapping[x], ciphertext))
# plaintext = reduce(lambda x,y: x.replace(y, mapping[y]), mapping.keys(), ciphertext)
print(sorted(counts.items(), key=lambda x: x[1], reverse=True))
print()
print(ciphertext)
print()
print(plaintext)
print(''.join(['_' if ord(mapping[c]) >= ord('a') else c for c in ciphertext]))
print()
print(''.join(['_' if ord(c) < ord('a') else c for c in plaintext]))

# shifts = [5, 3, 9, 8, 4, 4, 4, 9]
# for i, c in enumerate(ciphertext):
#     shift = shifts[i%8]
#     print(chr((ord(c) - ord('A') + shift) % 26 + ord('A')), end="")

# factors = [2, 3, 4, 6, 12, 37, 74, 111, 148, 222]
# for f in factors:
#     table = [ciphertext[i:i+f] for i in range(0, 444, f)]
#     for row in table: print(row)
#     print()
