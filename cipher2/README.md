* Choose ciphertext to decode by modifying `ciphertext` variable in `main.cpp`
* Choose initial key by modifying `key` variable in `main.cpp`
* Compile with `g++ -o main main.cpp playfair.cpp simulatedannealing.cpp`
* Run with `./main`
