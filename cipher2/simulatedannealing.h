#pragma once

#include <functional>

#include "playfair.h"

class SimulatedAnnealing {
public:
    SimulatedAnnealing(Playfair& c, std::function<double(std::string)> computeFitness,  double startTemp, double tempStep, int nIter, std::string ciphertext);

    double run(std::string& initKey);
    bool step(double t, const std::string& key);

    double fitness;
    double maxFitness;
private:
    Playfair& cipher;
    std::function<double(std::string)> computeFitness;
    double startTemp;
    double tempStep;
    int nIter;
    std::string ciphertext;
};