#include <cmath>
#include <limits>
#include <iostream>

#include "simulatedannealing.h"

SimulatedAnnealing::SimulatedAnnealing(Playfair& c, std::function<double(std::string)> computeFitness, double startTemp, double tempStep, int nIter, std::string ciphertext) 
: cipher(c) {
    this->computeFitness = computeFitness;
    this->startTemp = startTemp;
    this->tempStep = tempStep;
    this->nIter = nIter;
    this->ciphertext = ciphertext;
    this->fitness = -1e100;
    this->maxFitness = -1e100;
}

double SimulatedAnnealing::run(std::string& initKey) {
    std::string parentKey = initKey;
    std::string childKey;
    childKey.resize(parentKey.length());
    for (double t = startTemp; t > 0; t -= tempStep) {
        cipher.randomizeKey(parentKey, childKey);
        if (step(t, childKey)) {
            parentKey = childKey;
        }
    }
    initKey = parentKey;
    return maxFitness;
}

bool SimulatedAnnealing::step(double t, const std::string& key) {
    std::string plaintext = cipher.decrypt(ciphertext, key);
    double newFitness = computeFitness(plaintext);
    double dF = newFitness - fitness;
    if (newFitness > maxFitness) {
        std::cout << "Improved fitness: " << maxFitness << " -> " << newFitness << std::endl;
        maxFitness = newFitness;
        cipher.printKey(key);
        std::cout << plaintext << std::endl << std::endl;
    }
    if (dF > 0) {
        fitness = newFitness;
    } else {
        double prb = std::exp(dF / t);
        if ((((double) rand()) / RAND_MAX) < prb) {
            fitness = newFitness;
        } else {
            return false;
        }
    }
    return true;
}