#include <algorithm>
#include <iostream>
#include <map>
#include <string>

#include "playfair.h"

void Playfair::randomizeKey(const std::string& parentKey, std::string& childKey) {
    int r = rand() % 50;
    int i = rand() % 5;
    int j = rand() % 5;
    childKey = parentKey;
    switch (r) {
        case 0: // swap rows
            for (int c = 0; c < 5; c++) {
                std::swap(childKey[i*5+c], childKey[j*5+c]);
            }
            break;
        case 1: // swap cols
            for (int c = 0; c < 5; c++) {
                std::swap(childKey[c*5+i], childKey[c*5+j]);
            }
            break;
        case 2: // reverse key
            for (int c = 0; c < 12; c++) {
                std::swap(childKey[c], childKey[24-c]);
            }
            break;
        case 3: // flip horizontal
            for (int c = 0; c < 5; c++) {
                std::swap(childKey[c*5+0], childKey[c*5+4]);
                std::swap(childKey[c*5+1], childKey[c*5+3]);
            }
            break;
        case 4: // flip vertical
            for (int c = 0; c < 5; c++) {
                std::swap(childKey[0*5+c], childKey[4*5+c]);
                std::swap(childKey[1*5+c], childKey[3*5+c]);
            }
            break;
        default: // swap elements
            int k = rand() % 5;
            int l = rand() % 5;
            std::swap(childKey[i*5+j], childKey[k*5+l]);
            break;
    }
}

void Playfair::printKey(const std::string& key) {
    for (int i = 0; i < 5; i++) {
        for (int j = 0; j < 5; j++) {
            std::cout << key[i*5+j] << " ";
        }
        std::cout << std::endl;
    }
}

std::string Playfair::encrypt(const std::string& plaintext, const std::string& key) {
    std::string ciphertext;
    ciphertext.resize(plaintext.length());

    std::map<char, int> keyPos;
    for (int i = 0; i < 25; i++) {
        keyPos[key[i]] = i;
    }

    for (int i = 0; i < plaintext.length(); i+=2) {
        char p1 = plaintext[i];
        char p2 = plaintext[i+1];
        const int8_t pos1 = keyPos[p1];
        const int8_t pos2 = keyPos[p2];
        const int8_t p1_row = pos1 / 5;
        const int8_t p1_col = pos1 % 5;
        const int8_t p2_row = pos2 / 5;
        const int8_t p2_col = pos2 % 5;
        char c1, c2;
        if (p1_row == p2_row) {
            c1 = key[p1_row * 5 + (p1_col + 1) % 5];
            c2 = key[p2_row * 5 + (p2_col + 1) % 5];
        } else if (p1_col == p2_col) {
            c1 = key[((p1_row + 1) % 5) * 5 + p1_col];
            c2 = key[((p2_row + 1) % 5) * 5 + p2_col];
        } else {
            c1 = key[p1_row * 5 + p2_col];
            c2 = key[p2_row * 5 + p1_col];
        }
        ciphertext[i] = c1;
        ciphertext[i+1] = c2;
    }
    return ciphertext;
}

std::string Playfair::decrypt(const std::string& ciphertext, const std::string& key) {
    std::string plaintext;
    plaintext.resize(ciphertext.length());

    std::map<char, int> keyPos;
    for (int i = 0; i < 25; i++) {
        keyPos[key[i]] = i;
    }

    for (int i = 0; i < ciphertext.length(); i+=2) {
        char p1 = ciphertext[i];
        char p2 = ciphertext[i+1];
        const int8_t pos1 = keyPos[p1];
        const int8_t pos2 = keyPos[p2];
        const int8_t p1_row = pos1 / 5;
        const int8_t p1_col = pos1 % 5;
        const int8_t p2_row = pos2 / 5;
        const int8_t p2_col = pos2 % 5;
        char c1, c2;
        if (p1_row == p2_row) {
            c1 = key[p1_row * 5 + (p1_col + 4) % 5];
            c2 = key[p2_row * 5 + (p2_col + 4) % 5];
        } else if (p1_col == p2_col) {
            c1 = key[((p1_row + 4) % 5) * 5 + p1_col];
            c2 = key[((p2_row + 4) % 5) * 5 + p2_col];
        } else {
            c1 = key[p1_row * 5 + p2_col];
            c2 = key[p2_row * 5 + p1_col];
        }
        plaintext[i] = c1;
        plaintext[i+1] = c2;
    }
    return plaintext;
}
