#pragma once

#include <string>

class Playfair {
public:
    void randomizeKey(const std::string& parentKey, std::string& childKey);
    void printKey(const std::string& key);

    std::string encrypt(const std::string& plaintext, const std::string& key);
    std::string decrypt(const std::string& ciphertext, const std::string& key);
};