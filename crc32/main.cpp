#include <iomanip>
#include <iostream>
#include <chrono>
#include <random>
#include <unordered_map>

#include "crc32.h"

std::string to_hex_string_64(uint64_t x) {
    std::stringstream ss;
    ss << "0x" << std::setw(16) << std::setfill('0') << std::hex << x;
    return ss.str();
}

std::string to_hex_string_crc(crc_t c) {
    std::stringstream ss;
    ss << "0x" << std::setw(8) << std::setfill('0') << std::hex << c;
    return ss.str();
}

int main() {
    // set up random number generator
    std::random_device device;
    std::mt19937_64 gen(device());
    std::uniform_int_distribution<uint64_t> rand64;

    // create timer

    auto start = std::chrono::steady_clock::now();
    std::unordered_map<crc_t, uint64_t> hashes;
    hashes.reserve(200000);
    while (true) {
        uint64_t data = rand64(gen);
        crc_t crc = crc_finalize(crc_update(crc_init(), &data, 8));
        if (hashes.find(crc) != hashes.end()) {
            std::cout << "Hash collision between "
                      << to_hex_string_64(__builtin_bswap64(data))
                      << " and "
                      << to_hex_string_64(__builtin_bswap64(hashes[crc]))
                      << std::endl;
            break;
        } else {
            hashes.insert({crc, data});
        }
    }
    auto end = std::chrono::steady_clock::now();
    std::cout << "Took " << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() << "us" << std::endl;
    std::cout << std::endl;

    start = std::chrono::steady_clock::now();
    const uint8_t stdnum[] = {0xde, 0xf3, 0x81, 0xd7, 0x4c, 0xde, 0x04, 0x58, 0xe2, 0xfd, 0x8d, 0x7d, 0x13, 0x76, 0x2b, 0x1b};
    crc_t stdcrc = crc_finalize(crc_update(crc_init(), stdnum, 16));
    std::cout << "Student number hashes to " << to_hex_string_crc(stdcrc) << std::endl;
    while (true) {
        uint64_t data = rand64(gen);
        crc_t crc = crc_finalize(crc_update(crc_init(), &data, 8));
        if (crc == stdcrc) {
            std::cout << "Hash collision with student number: " << to_hex_string_64(__builtin_bswap64(data)) << std::endl;
            break;
        }
    }
    end = std::chrono::steady_clock::now();
    std::cout << "Took " << std::chrono::duration_cast<std::chrono::seconds>(end - start).count() << "s" << std::endl;

    return 0;
}